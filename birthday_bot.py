import datetime
import time
import apscheduler
import http.client
from datetime import datetime
from datetime import date
from datetime import timedelta
from apscheduler.schedulers.blocking import BlockingScheduler


bdays = { 
            "Birthday 1":"01-01",
            "Birthday 2":"01-03"
        }

def getBirthdays():
    today = str(date.today())
    twoDaysFromNow = str(date.today() + timedelta(days=2))
    yearToday = today[0:5] # "XXXX-"
    yearTwoDaysFromNow = twoDaysFromNow[0:5] # Needed in the case where the year changes 2 days from now
    bday_kids_today = []
    bday_kids_twoDaysFromNow = []
    for key in bdays:
        bday_today = str(yearToday + bdays[key])
        bday_twoDaysFromNow = str(yearTwoDaysFromNow + bdays[key]) 
        if (bday_today == today):
            bday_kids_today.append(key)
        elif (bday_twoDaysFromNow == twoDaysFromNow):
            bday_kids_twoDaysFromNow.append(key)
    bday_kids = [bday_kids_today, bday_kids_twoDaysFromNow]
    return bday_kids
    
def createMessage():
    bday_kids = getBirthdays()
    bday_kids_today = bday_kids[0]
    bday_kids_twoDaysFromNow = bday_kids[1]
    message = ""
    if (len(bday_kids_today) > 0):
        message = message + "Today's birthday(s): "
        for i in range(0,len(bday_kids_today)):
            message = message + "\n" + bday_kids_today[i]
        message = message + "\n\n"
    
    if (len(bday_kids_twoDaysFromNow) > 0):
        message = message + "Upcoming birthday(s)"
        twoDaysFromNow = str(date.today() + timedelta(days=2))
        message = message + " (" + twoDaysFromNow + "): "
        for i in range(0,len(bday_kids_twoDaysFromNow)):
            message = message + "\n" + bday_kids_twoDaysFromNow[i]
    return message



def send(message):
    # your webhook URL
    webhookurl = "YOUR-WEBHOOK-URL"
 
    # compile the form data (BOUNDARY can be anything)
    formdata = "------:::BOUNDARY:::\r\nContent-Disposition: form-data; name=\"content\"\r\n\r\n" + message + "\r\n------:::BOUNDARY:::--"
  
    # get the connection and make the request
    connection = http.client.HTTPSConnection("discord.com")
    connection.request("POST", webhookurl, formdata, {
        'content-type': "multipart/form-data; boundary=----:::BOUNDARY:::",
        'cache-control': "no-cache",
        })
  
    # get the response
    response = connection.getresponse()
    result = response.read()
  
    # return back to the calling function with the result
    return result.decode("utf-8")


sched = BlockingScheduler()
time = 15 # desired time
@sched.scheduled_job('cron', day_of_week='mon-sun', hour=time)
def scheduled_job():
    message = createMessage()
    if (message != ""):
        print("Attempting message send...")
        # send the messsage and print the response
        print(send(message))

sched.start()